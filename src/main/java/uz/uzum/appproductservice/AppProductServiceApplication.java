package uz.uzum.appproductservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppProductServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppProductServiceApplication.class, args);
    }

}
