package uz.uzum.appproductservice.repository;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import uz.uzum.appproductservice.entity.Product;

@Repository
public interface ProductRepository extends MongoRepository<Product, String> {

}
