package uz.uzum.appproductservice.controller;

import jakarta.ws.rs.Path;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import uz.uzum.appproductservice.entity.Product;
import uz.uzum.appproductservice.repository.ProductRepository;
import uz.uzum.appproductservice.utils.AppConstants;

import java.util.List;

@RestController
@RequestMapping(AppConstants.BASE_PATH + "/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductRepository productRepository;


    @GetMapping
    public List<Product> list() {
        return productRepository.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Product add(@RequestBody Product product) {
        return productRepository.save(product);
    }

    @PostMapping("/by-ids")
    public List<Product> listByIds(@RequestBody List<String> productIds) {
        return productRepository.findAllById(productIds);
    }

    @GetMapping("/{id}")
    public Product one(@PathVariable String id) {
        return productRepository.findById(id).orElseThrow();
    }
}
